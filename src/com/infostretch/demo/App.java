package com.infostretch.demo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class App {
	public static void main(String[] args) throws IOException {
		Student student = new Student("Abhi", 27);
		FileOutputStream outputStream = new FileOutputStream("f.txt");
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
		objectOutputStream.writeObject(student);
		objectOutputStream.flush();
		objectOutputStream.close();
		outputStream.close();
		System.out.println("success");
	}
}
