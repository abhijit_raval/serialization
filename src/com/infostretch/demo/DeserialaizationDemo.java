package com.infostretch.demo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserialaizationDemo {
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream inputStream=new ObjectInputStream(new FileInputStream("f.txt"));
		Student s=(Student) inputStream.readObject();
		System.out.println(s.id +"=>"+s.name);
	}
}
